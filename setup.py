from setuptools import setup

with open('requirements.txt', 'r') as handle:
    requirements = [line.strip() for line in handle.readlines()]

with open('VERSION') as handle:
    version = handle.read().strip()

setup(
    name='bunt',
    version=version,
    packages=['bunt', 'bunt.containers'],
    scripts=['bin/bunt'],
    install_requires=requirements
)
