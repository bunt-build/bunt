FROM python:3.7
ENV PYTHONUNBUFFERED=1

# Install requirements of bunt
COPY requirements.txt /code/requirements.txt
RUN pip3 install -r /code/requirements.txt


# Install bunt
COPY VERSION /code/VERSION
COPY bin/ /code/bin
COPY bunt/ /code/bunt
COPY setup.py /code/setup.py
RUN pip3 install /code/ && \
    rm -r /code/

# Install scut
RUN pip install scut>=0.0.1

ENTRYPOINT [""]

