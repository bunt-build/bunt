![](assets/branding.png)
# Bunt: Simplified Integration Testing


This tool provides a cli for running, and a set of abstractions centered around, integration
testing. You can learn more by reading our [documentation](documentation/README.md).

## Current Features

1. Automatic setup and tear down of integration test environments
    * Flat network that enables talking to each service as if it was on it’s own machine
    * No need to manually assign ports for each service
2. Databases with mainline support and configurations provided
    * Pre-made sugar for databases and services
3. High-level testing abstractions
    * Wrapper for HTTP APIs
    * Wrapper for RESTful APIs that can optionally enforce good restful practices 
4. Distributed test running through the `hair` tool (released seperately).
    * Completely free software
    * Entirely selfhosted
    * Simple to deploy


## Planned Features

1. Automatic Code Coverage for Dynamic Runtime Languages
    * Attaching debuggers to processes running within containers
    * Aggregate trace information into a daemon
    * I belive this is possible for: Python, Java, NodeJS, and a few others
2. Benchmarking
    * Load testing HTTP requests
    * Load testing TCP sockets and connections
    * Load testing UDP datagrams
3. Resource utilization tracking
    * Memory
    * CPU
    * Disk
    * Network
4. Service Mocking 
    * Anything: Intercept and respond to TCP/UDP messages from your tests
    * HTTP: Generate mock services from configuration or code
    * gRPC: Generate mock services from .protos, configs, or code

## Possible Features

1. Long running feature branch testing environments?
    * Spawn all code for a limited time period
    * Expose all services over ephemeral domain names.
2. Distributed container builds?
3. Test harnesses for multiple languages?
    * Java
    * Golang
    * C#
4. Multiple operating systems?
    * Windows containers?
    * MacOS Sandbox/Jails?
    * BSD Jails?
5. Multiple CPU Architectures
    * ARM?
    * RISC-V?
6. Matrix support
    * Run tests and builds with large matrix of machine and config options
7. Special hardware?
    * GPUs?
    * FPGAs?
    * ASICs?
8. Graphical and User Interfaces Support?
    * X Server with programatically-controllable hands?
    * Software emulated Vulkan, OpenGL, and DX support for testing games with render pipelines?
9. Runner qutoas and limits 
    * Restrict memory
    * Restrict cpu
    * Restrict disk
    * Restrict network
    * Restrict total runtime


## What 

Bunt is an integration testing framework designed for engineers who find themselves:

1. Maintaining a legacy project with no existing test suite, 
2. Find themselves in charge of complex service-to-service systems integration work, 
3. Deploying broken builds or configuration despite having good coverage of unit tests,
4. Needing multiple databases and services to correctly test an application,
5. Or wanting to have language-agnostic verification of service functionality for green-fields.

## Why

Looking around the open source community there is an obvious need to have some form of integration 
testing tooling. There are existing solutions that do a good job of augmenting an existing testing
workflow but there does not seem to be any tool that offers an opinionated end-to-end integration
testing workflow. Because of this there is very low adoption of standardized integration testing
tooling and many projects end up reimplementing some version of an integration testing framework
by way of hacky bash scripts to even hackier - and complex - Makefiles. I wanted something that
would:

- Be fast (sub-10-second) to do simple tests,
- Be easy to read and understand with abstractions that remove boilerplate,
- Enabled me to test code paths that cannot be tested in a unit test, 
- Allowed me to validate integration with code or services I don’t own,
- Removed barriers preventing me from testing code that could not be restructured to be easily testable without a lot of mocking and coupling of implementation and test, 
- Unified the testing experience in code bases and teams that are heterogeneous,
- And standardizing the way I look at integration tests.

I think bunt is a tool that provides these feature sets while reducing the barrier to entry for
setting up integration tests.


# Mascot

I'm not an artist.

![](assets/mascot.png)