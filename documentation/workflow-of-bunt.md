# Using the Bunt CLI

The CLI for bunt manages setting up a lot of the steps talked about in [the overview](overview-of-bunt-internals.md).
In this section we will cover the CLI, how to use it, and a high level overview of one workflow that bunt enables.

## The CLI Commands

1. `bunt list`: List the tests bunt sees.
    * `--directory/-d`: Directory to load tests from
        - Defaults to CWD
2. `bunt run <test path>`: Run a test in a bunt environment.
    * `--directory/-d`: Directory to load tests from
        - Defaults to CWD
    * `--network`: The name of the docker bridge network we will create.
        - Defaults to `bunt`
    * `--network_cleanup`: If set we delete the network at the end of the tests.
        - Defaults to `False`
    * `--image`: The image to use to run your bunt tests in.
        - Environment variable `BUNT_IMAGE`
        - Defaults to `registry.gitlab.com/bunt-build/bunt:<bunt-version>`

## Basic Workflow

Suppose you have a Dockerize microservice that looks something like this:

```
➜  bunt ls -1
Dockerfile
library
```

You can build this image on your system. For example you might do:

```
➜  bunt docker build -t library:latest .                            
Sending build context to Docker daemon  24.58kB
.. build ..
Successfully built 9f5a870f5311
Successfully tagged library:latest
```

You can now start writing bunt tests. To do this it is very helpful to create a
folder dedicated to testing your application. I'm going to call mine "integrations"
as it will contain the integration tests for my application. I'm calling mine
"integrations" as opposed to "tests" as in the future I might want to have both
a unit and an integration test suite. My directory structure now looks something
like:

```
➜  bunt ls -1 
Dockerfile
integrations
library
```

Inside of this new folder I am going to create three files:
1. An `__init__.py`: This is just a file python needs to make a folder importable. (can be automated in the future)
2. A `containers.py`: I will define my bunt containers inside here so multiple test files can import and use the same
                      bunt container definitions
3. A `test_library.py`: This is where I will write my tests. The file must start with `test_` and end in `.py`. I am
                        calling mine `test_library` because I am `test`ing the `library` service. If you have many
                        complex test cases to test it would be a good idea to break up your tests in multiple files. If
                        you are working with a monorepo it might be good to have a single test file for each service. 
                        Dealers choice. 

My directories now look like this:

```
➜  bunt ls -1 
Dockerfile
integrations
library
➜  bunt ls -1 integrations 
containers.py
__init__.py
test_library.py
```

Now, lets start defining some containers for our tests. I'll start by adding to `integrations/containers.py` the
following:

```python
from bunt.containers import ContainerConfigurationRest
from bunt.containers import ContainerConfigurationHttpHealthCheck


class LibraryContainer(ContainerConfigurationRest, ContainerConfigurationHttpHealthCheck):
  image = 'library:latest'
  host_name = 'library'
  http_port = 8080
  tcp_ports = frozenset({8080, })
  http_health_check_path = '/'
```

I happen to know this is the correct set of configuration options for the code in `library`. The container
binds and serves HTTP traffic on 8080. The server also does not have a `/healthz` endpoint so I am overriding
that to `/`. That step is not explicitly needed since `/` and `/healthz` will both return 404 in library. I am
just doing this to keep track of what services do or do not have health checks explicitly implemented this way
I know what to clean up down the line.

Lets go ahead and start writing a simple test case for this. I'm going to add into `integrations/test_library.py` the
following: 

```python
import unittest
from bunt.wrapper import Bunt

# `from integrations` is our import path because we named our folder `integrations`
from integrations.containers import LibraryContainer


# This allows bunt to do some setup logic it needs.
bunt = Bunt()


class LibraryTestCase(unittest.TestCase):
    @bunt.test_case([LibraryContainer])  # Tells bunt to start up our LibraryContainer before calling our code
    def test_create_book(self, library: LibraryContainer):  # The type hint is used to inject the container instance.
        book = library.create('/books', json={  # Using the methods of LibraryContainer (from the Rest sugar) to create
            "title": "How to use Bunt",
            "author": "Joshua Katz",
        })

        # Validating the returned format is as we expect
        self.assertTrue('id' in book)
        self.assertTrue('title' in book)
        self.assertTrue('author' in book)

        # Validating the values we assigned are as we expect
        self.assertEqual('How to use Bunt', book['title'])
        self.assertEqual('Joshua Katz', book['author'])
```

Now that we have created a container definition lets try and list the tests
bunt sees. We do this by executing:


```
➜  bunt # Correctly executing the tool from the root of the project.
➜  bunt bunt -d integrations list
integrations.test_library.LibraryTestCase.test_create_book

➜  bunt # You can also execute the tool from inside the `integrations` folder. 
➜  bunt bunt list
integrations.test_library.LibraryTestCase.test_create_book

➜  bunt # Some issues that you are very likely to run into....
➜  bunt # If you made a mistake and forgot to include `-d` when in the root of the project you will see...
➜  bunt bunt list
Traceback (most recent call last):
  File "/home/gravypod/.local/bin/bunt", line 145, in <module>
    sys.exit(main())
  File "/home/gravypod/.local/bin/bunt", line 137, in main
    status = commands[args.command](args)
  File "/home/gravypod/.local/bin/bunt", line 38, in command_list
    for test_case_id in sorted(bunt_list(directory)):
  File "/home/gravypod/.local/bin/bunt", line 22, in bunt_list
    top_level_dir=str(test_code_folder.parent)
  File "/usr/lib/python3.7/unittest/loader.py", line 346, in discover
    raise ImportError('Start directory is not importable: %r' % start_dir)
ImportError: Start directory is not importable: '/home/yourusername/path/to/bunt'

➜  bunt # If you forgot to create the __init__.py inside your integrations folder
➜  bunt bunt -d integrations list
Traceback (most recent call last):
  File "/home/gravypod/.local/bin/bunt", line 145, in <module>
    sys.exit(main())
  File "/home/gravypod/.local/bin/bunt", line 137, in main
    status = commands[args.command](args)
  File "/home/gravypod/.local/bin/bunt", line 38, in command_list
    for test_case_id in sorted(bunt_list(directory)):
  File "/home/gravypod/.local/bin/bunt", line 22, in bunt_list
    top_level_dir=str(test_code_folder.parent)
  File "/usr/lib/python3.7/unittest/loader.py", line 346, in discover
    raise ImportError('Start directory is not importable: %r' % start_dir)
ImportError: Start directory is not importable: '/home/yourusername/path/to/bunt/integrations'

➜  bunt # If theres an syntax error with the integration test code
➜  bunt bunt -d integrations list
unittest.loader._FailedTest.integrations.test_library

```

Now that we know what tests we've defined lets try running it using the `bunt run` command:

```
➜  bunt bunt -d integrations run integrations.test_library.LibraryTestCase.test_create_book
Watching logs...
test_create_book (integrations.test_library.LibraryTestCase) ... ok

----------------------------------------------------------------------
Ran 1 test in 2.351s

OK
```

If you want to run all of the test cases you've defined you can run then by doing the following:

```
➜  bunt bunt -d integrations run integrations.test_library.LibraryTestCase                 
Watching logs...
test_hypothetical_test_1 (integrations.test_library.LibraryTestCase) ... ok
test_hypothetical_test_2 (integrations.test_library.LibraryTestCase) ... ok
test_hypothetical_test_3 (integrations.test_library.LibraryTestCase) ... ok
test_create_book (integrations.test_library.LibraryTestCase) ... ok

----------------------------------------------------------------------
Ran 1 tests in 3.231s

OK
```

And with that you've done it all! You've

1. Built your container
2. Created an integration test module (`integrations/`)
3. Made your test module importable (`integrations/__init__.py`)
4. Defined the containers you want to test (`integrations/containers.py`)
5. Defined some test cases (`integrations/test_library.py`)
6. Listed your available tests (`bunt -d integrations/ list`)
7. Run your test/tests (`bunt -d integrations/ run <test case>`)

You're ready to start using bunt to test your code!