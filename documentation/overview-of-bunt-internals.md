# Down the Bunt Rabbit Hole

As it currently stands bunt is, essentially, syntax sugar 
sprinkled on top of `dockerd`'s API and Python's `unittest`
module. In the future these dependencies will become more
modular and bunt will support multiple container runtime 
environments and multiple test definition languages and 
libraries. As long as all of the containers you need for 
your tests exist on your machine you can run any bunt
test without any internet connection or external servers.

# How Bunt Creates a Test Environment

When you run a test, using `bunt run some.ServiceTestCase`,
bunt goes through multiple steps.

1. Connect to `dockerd`: Bunt opens a connection to a docker daemon. This docker daemon
                         can run locally or remotely. Bunt is configured to use the
                         `DOCKER_*` to be configured for how to talk to docker.
2. Make a docker `Network`: Bunt creates a docker bridge network for your tests and
                            containers to run in. By default it calls this network
                            `bunt` but this can be overridden by a cli argument
                            `--network`. This network is not automatically deleted
                            unless the cli argument `--network_cleanup` is also
                            passed. This is done as a (minor) performance
                            optimization. 
3. Make a `ContainerConfiguration`: Bunt uses it's own container configuration language
                                    to define a container for your tests to run in. By
                                    default it starts an image provided by bunt. This
                                    can be overridden by an environment variable 
                                    (`BUNT_IMAGE`) or a cli argument `--image`. 
4. Volume mounts your tests: Bunt maps your tests into this test container. It does this
                             by taking the folder you store your tests in and mounting 
                             that directory into the test container under `/code/`. Bunt
                             defaults to assuming your tests are stored in your CWD. This
                             can be overridden with a cli argument `--directory/-d`. If you
                             have a folder `./integration_tests` and you run 
                             `bunt -d integration_tests ...` bunt will mount that folder into
                             the container under `/code/integration_tests`. The provided
                             bunt test container sets the `PYTHONPATH` environment variable
                             to include `/code` so any tests you write can import from that
                             folder as a module (ex: `from tegration_tests.containers ...`).
                             In the future we can use this to support multiple test suites at
                             the same time as well. We also mount the host's docker socket
                             (`/var/run/docker.sock`) into the container. This is used to
                             allow the bunt library to start, stop, and inspect containers.  
5. Start `unittest` in the test container: Bunt executes `python3 -m unittest <test>` inside the test container. This
                                           causes unittest to load your module and start executing your test cases.
6. Your test runs `bunt.test_case`: Now that your integration test module is loaded you make use of the bunt library
                                    by adding a decorator function to any of your test cases. When the wrapper provided
                                    to by the decorator is called your containers are started, dependency injection for
                                    your tests is handled, and then your containers are torn down regardless of what
                                    happened within your test. We also automagically call `close()` on any container
                                    or container connection that is created so you don't have to worry about warnings
                                    from leaked DB connections or requests sessions.
7. Bunt pipes the test container's STDOUT/ERR to your terminal and waits for your container to exit
8. Bunt exits with the same status code as your container.

## Why Does Bunt Volume Mount my Tests?

This added complexity exists for a few reasons:

1. Minimize test iteration time: No need to create a Dockerfile, build, and tag your integration tests locally.
2. Reduced layer sizes for test runners: If the default of the tool is to run tests in a standardized environment then
                                         most people will write their code to run within that environment. When those
                                         same engineers go to Dockerize their tests to run in a distributed test runner
                                         system the default container will make an obvious source to `FROM` in your
                                         Dockerfile. This reduces the amount of data that needs to be sent to the 
                                         runners since the, sometimes large, libraries and configurations needed to
                                         connect to databases or interface with systems will be pre-packaged into a base
                                         layer. 
 
## Why Does Bunt run my Code in a Docker Bridge Network?

When your code is running in your production environment you usually have unique names for each service that is running
within your network. Maybe you have a `db.internal.com` that resolves on your network. You might also have a
`website.internal.com` that accepts HTTP requests and talks to your database. Bunt is designed to simulate these
environments. Bunt puts your tests and containers into a docker bridge so that these services can behave as they would
in your production environment. If you needed to run two databases (`users-db` and `orders-db`) you would traditionally
run these on two ports in your testing environment and connect to these databases over `localhost` rather than their
DNS names in your production environment. In my career so far I have seen many errors arise from this dev/prod
inconsistency. Within the docker bridge network each container gets it's own fake IP address and can open whatever ports
it wants to. This lets you more closely resemble your production environment in your test suite.

## Will Bunt Support Different Languages for Writing Tests in?

I think that is the best direction for this project to go. I feel most comfortable with python's magic so I implemented
this first test suite and sugar in it. Because of how these the `run` command works you could theoretically use any
language. If anyone is interested in supporting more languages I'd love to assist in any way possible. 
