# Containers in Bunt

We use python classes to define containers that bunt will spawn
for testing. These classes configure the setup and topology of
your testing environment.

## Defining a Simple Container 

This is an example of defining a very simple container in bunt
and will run a very simple http server.

```python
from bunt.containers import ContainerConfiguration

class WebsiteContainer(ContainerConfiguration):
    # Docker image tag to run.
    image = 'website:latest'

    # DNS of this container within the network.
    host_name = 'website'

    # This container exposes this port.
    tcp_ports = frozenset({80, })

    # Will execute "/simple-httpd --root /var/www" within the container.
    entry_point = ['/webserverd']
    command     = ['--root', '/var/www']

    # Will set these environment variables within the container.
    environment_variables = {
        'WEBSERVERD_PORT': '80',
        'WEBSERVERD_ADDRESS': '0.0.0.0'
    }
```

# Container Configuration Mixins

We provide a few mixin classes that can be used to remove some boilerplate
in your container definition code. All of these are importable using
`from bunt.containers import <MixInName>`.

## Mixin: bunt.containers.ContainerConfiguration

Base configuration for any container. 

**Available Options**
* `name`: Optional name of the container.
* `image`: Docker container we would like to run.
* `host_name`: The internal network DNS name of this service.
* `environment_variables (Dict[str, str])`: Environment variables mapping for the container.
* `entry_point (Optional[List[str]])`: Command to execute inside the container when the container starts.
* `command (Optional[List[str]])`: Arguments given to the `entry_point` command. 
* `tcp_ports (FrozenSet[int])`: Ports that this container exposes.
* `depends_on (FrozenSet[Type[ContainerConfigurations]])`: Defines containers that need to be running before we start.
* `volume_mounts (Dict[str, str])`: Don't use this, likely to change. Mount files into the container at runtime. 

**Example**
```python
from bunt.containers import ContainerConfiguration

class DatabaseContainer(ContainerConfiguration):
    ...

class WebsiteContainer(ContainerConfiguration):
    # Docker image tag to run.
    image = 'website:latest'

    name = 'website'

    # DNS of this container within the network.
    host_name = 'website'

    # This container exposes this port.
    tcp_ports = frozenset({80, })

    # Will execute "/simple-httpd --root /var/www" within the container.
    entry_point = ['/webserverd']
    command     = ['--root', '/var/www']

    # Will set these environment variables within the container.
    environment_variables = {
        'WEBSERVERD_PORT': '80',
        'WEBSERVERD_ADDRESS': '0.0.0.0'
    }

    # We will only start this container after DatabaseContainer has started
    depends_on = frozenset({DatabaseContainer, })
```

## Mixin: bunt.containers.ContainerConfigurationHealthCheck

This mixin provides some configuration for service health checks. We use
healtchecks to make sure a service has started correctly. Your tests, and 
dependant services, will not be run unless your service passes it's health
check.


**Available Options**
* `health_check_maximum_retries (int)`: Maximum number of times to try the health check before giving up.
* `health_check_time_out (float)`: Seconds before marking a container as dead.

**Required Function Definitions**
* `def health_check(cls, **kwargs) -> bool`: A `@classmethod` that returns True if your container is healthy.

**Example**
```python
from bunt.containers import ContainerConfiguration
from bunt.containers import ContainerConfigurationHealthCheck
import requests

class WebsiteContainer(ContainerConfiguration, ContainerConfigurationHealthCheck):
    image = 'website:latest'
    host_name = 'website'
    tcp_ports = frozenset({80, })

    # Try 10 times before giving up
    health_check_maximum_retries = 10

    # Try for a maximum of 4 seconds before giving
    health_check_time_out = 4


    @classmethod
    def health_check(cls, **kwargs) -> bool:
        try:
            if requests.get('http://website:80/some-path').ok:
                # If an HTTP request succeeds this container is healthy
                return True
        except ConnectionError:
            pass
        return False

```

## Mixin: bunt.containers.ContainerConfigurationClientConnectable

Some systems are very complicated and require special code to connect to and 
drive the interaction process. Constructing those objects often requires a lot
of information about the configuration of the service you intend to talk to and,
in a testing environment, is likely a very common process. To simplify this we have
the `ContainerConfigurationClientConnectable`. 

If `ContainerConfigurationClientConnectable` and `ContainerConfigurationHealthCheck` 
are implemented for a `ContainerConfiguration` bunt guarantees it will not attempt
to call the `connection()` function until the service passes it's health check. This
can be useful for clients that will throw exceptions or hang if the service it is
interfacing with is not online. 

**Available Options**
* `connectableconnectable_container_type (Type)`: The class/type of our connection driver.

**Required Function Definitions**
* `def connection(cls, **kwargs)`: A `@classmethod` creates, and configures, the defined type in such a way that it will
  interface with the defined container

**Example**
```python
from bunt.containers import ContainerConfigurationClientConnectable
from bunt.containers import ContainerConfiguration
from bunt.wrapper import Bunt
import unittest
import requests

class WebsiteContainerClient:
    def __init__(self, url: str, api_key: str):
        self.url = url
        self.api_key = api_key

    def list(self) -> list:
        return requests.get(f'{self.url}/some-endpoint', headers={
            'authorization': self.api_key
        }).json()



class WebsiteContainer(
    ContainerConfiguration,
    ContainerConfigurationClientConnectable,
):
    image = 'website:latest'
    host_name = 'website'
    tcp_ports = frozenset({80, })

    connectable_container_type = WebsiteContainerClient

    environment_variables = {'API_KEY': '1234'}

    @classmethod
    def connection(cls, **kwargs):
        return WebsiteContainerClient(f'http://{cls.host_name}:80', cls.environment_variables['API_KEY'])

bunt = Bunt()

class MyTestCase(unittest.TestCase):
    # As you can see the WebsiteContainerClient is automatically injected into this test case.
    @bunt.test_case([WebsiteContainer])
    def test_website_container_driver(self, website_container_client: WebsiteContainerClient):
        self.assertIsInstance(website_container_client.list(), list)
``` 

## Mixin: bunt.containers.ContainerConfigurationHttp

HTTP is one of the worlds most popular application-layer protocols. Because 
bunt is built to test applications it comes with an abstraction that automates
most of the setup required, with mostly sane defaults, for HTTP servers. Internally
bunt sets up Requests and automatically prefixes the container's URI into any request
made. If you are not familiar with Python's requests module it is very simple to use
and a more detailed set of docs can be found [here](https://2.python-requests.org/en/master/). 


**Available Options**
* `http_protocol (str)`: The protocol part of the URL. Usually `http` or `https`.
* `http_port (int)`: The port the container is listening for HTTP requests on.


**Example**
```python
from bunt.containers import ContainerConfigurationHttp
from bunt.wrapper import Bunt
import unittest

# In this example website:latest is a distributed system that internally shards data.
# One such example of this would be something like ElasticSearch or Cassandra.
# Here we will define a base configuration and extend it. By using the 
# ContainerConfigurationHttp abstraction we don't manually need to juggle the different
# shard names within our test code. 
class WebsiteContainer(
    ContainerConfigurationHttp,
):
    image = 'website:latest'
    host_name = 'website'

    @classmethod
    def peers(cls):
        return ','.join([container.host_name for container in cls.__subclasses__()])

class WebsiteContainerShard1(WebsiteContainer):
    host_name = 'website1'
    environment_variables = {'SHARD': '1', 'PEERS': WebsiteContainer.peers()}

class WebsiteContainerShard2(WebsiteContainer):
    host_name = 'website2'
    environment_variables = {'SHARD': '2', 'PEERS': WebsiteContainer.peers()}

bunt = Bunt()

class MyTestCase(unittest.TestCase):
    @bunt.test_case([WebsiteContainerShard1, WebsiteContainerShard2])
    def test_website_container_driver(self, shard_1: WebsiteContainerShard1, shard_2: WebsiteContainerShard2):
        # Automatically dispatches request to the correct shard.
        response_1 = shard_1.http_session.get('/zone-data')
        response_2 = shard_2.http_session.get('/zone-data')

        # Test logic that validates how the shards should behave
        self.assertTrue(shard_1.ok)
        self.assertTrue(shard_2.ok)
```

## Mixin: bunt.containers.ContainerConfigurationHttpHealthCheck

Another popular feature of HTTP-driven services is health-checking at the 
application level. Since manually writing health checks, as shown in the
`bunt.containers.ContainerConfigurationHealthCheck` documentation would 
be tedious bunt includes abstractions to automate this process.

**Available Options**
* `http_health_check_path (str)`: The HTTP URI we will request to see if the service is alive. Defaults to `'/healthz'`.
* `http_health_check_acceptable_status_codes (List[int])`: HTTP status codes that mean the server is working.
                                                           Currently defaults to `[200, 201, 404]`.

**Example**
```python
from bunt.containers import ContainerConfigurationHttp
from bunt.containers import ContainerConfigurationHttpHealthCheck

class WebsiteContainerHttpAbstractions(ContainerConfigurationHttp, ContainerConfigurationHttpHealthCheck):
    image = 'website:latest'
    host_name = 'website'


from bunt.containers import ContainerConfiguration
from bunt.containers import ContainerConfigurationHealthCheck
import requests

class WebsiteContainerWithoutHttpAbstractions(ContainerConfiguration, ContainerConfigurationHealthCheck):
    image = 'website:latest'
    host_name = 'website'
    tcp_ports = frozenset({80, })

    @classmethod
    def health_check(cls, **kwargs) -> bool:
        try:
            if requests.get('http://website:80/some-path').ok:
                # If an HTTP request succeeds this container is healthy
                return True
        except ConnectionError:
            pass
        return False
```

## Mixin: bunt.containers.ContainerConfigurationRest

While HTTP is one of the most common protocols its usages vary widely from 
company to company. Many communication protocols have been developed on top
of HTTP. Notable examples are gRPC, JSON-rpc, and many other RPC frameworks.
One of the most commonly used communication patterns, often built on top of
HTTP, is a RESTful API implemented with JSON as a serialization mechanism.
This is the communication protocol I have used the most in my professional
career and as such I have provided a default abstraction for very common
RESTful communication patterns.  

This abstraction mixin is called `ContainerConfigurationRest` and it provides
helper functions for common REST operations. All of the functions defined in
this abstraction follow naming conventions for their arguments. For brevity 
the types of their arguments are listed here:

* `path`: A `str` or `Tuple[str]`. If tuple, the path is joined so that `('/thing', 10)` becomes `/thing/10`.
* `body`: A `bytes`, `str`, `IO`, or `bytearray`. This is sent as the body of the HTTP request.
* `json`: A `None`, `List[dict]`, `dict`, `str`, `float`, or `int`. Anything that can be sent to `json.dumps()`.
* `params`: A `dict` used to create URL params. So `{'a': 10}` becomes `?a=10` and `{'a': [1, 2]}` becomes `?a=1&a=2`.   

The included REST operations are:

* `create(path, body=None, json=None)`: Create a resource on this server.
* `get(path, params=None)`: Get a resource on this server.
* `update(path, body=None, json=None)`: Update a resource on this server.
* `delete(path)`: Delete a resource on this server.

We also include some not-explicitly-REST operations are:

* `list(path, params=None)`: List a collection of representations of state.
* `exists(path) -> bool`: Returns `True` if the representations of state exists 

All of the operations in this mixin also enforce certain HTTP/REST standards
that, ideally, people will follow when implementing these services. These
standards can be disabled with a boolean flag. I call these standards the 
"pedantic rest standards". Here is a brief overview of the mechanics of this
standard:

* `create()`
    1. Must return a dictionary (single representation of state)
    2. Can only accept a `body` or `json` parameter. No query string.
    3. Must have a URI with an odd number of parts.
        * `POST /things` is ok
        * `POST /things/10` is not ok
        * `POST /things/10/statueses` is ok
* `get()`
    1. Must return a dictionary (single representation of state)
    2. Can only accept a query string (`params`). No body.
    3. Must have a URI with an even number of parts.
        * `GET /things` is not ok
        * `GET /things/10` is ok
        * `GET /things/10/statueses` is not ok
* `update()` 
    1. Must return a dictionary (single representation of state)
    2. Can only accept a `body` or `json` parameter. No query string.
    3. Must have a URI with an even number of parts.
        * `PUT /things` is not ok
        * `PUT /things/10` is ok
        * `PUT /things/10/statueses` is not ok
* `delete()` 
    1. Must return a dictionary (single representation of state)
    2. Can not accept any query string or body data.
    3. Must have a URI with an even number of parts.
        * `DELETE /things` is not ok
        * `DELETE /things/10` is ok
        * `DELETE /things/10/statueses` is not ok
* `list()` 
    1. Must return a list of dictionaries (list of representations of state)
    2. Can only accept a query string (`params`). No body.
    3. Must have a URI with an odd number of parts.
        * `GET /things` is ok
        * `GET /things/10` is not ok
        * `GET /things/10/statueses` is ok
* `exists()` 
    1. Body's contents is never parsed
    2. Can not accept any query string or body data.
    3. Must have a URI with an even number of parts.
        * `GET /things` is not ok
        * `GET /things/10` is ok
        * `GET /things/10/statueses` is not ok
        
If these conventions do not work for your organization you can use the 
`request()` method on this mixin to completely avoid and of the 
function parameter restrictions placed on these operations. You can also
re-implement this class on top of the base `ContainerConfigurationHttp` as
the only complex thing this mixin does is enforce these constraints.

**Available Options**
* `rest_styling_pedantic (bool)`: If True we enforce the pedantic rest standard as defined above.

**Example**
```python
from bunt.containers import ContainerConfigurationRest
from bunt.containers import ContainerConfigurationHttpHealthCheck
from bunt.exceptions import ContainerRestNotFound
from bunt.wrapper import Bunt
import unittest


# Very simple CRUD app written using HTTP/JSON/REST
class WebsiteContainer(
    ContainerConfigurationRest,
    ContainerConfigurationHttpHealthCheck,
):
    image = 'website:latest'
    host_name = 'website'

bunt = Bunt()

class MyTestCase(unittest.TestCase):
    @bunt.test_case([WebsiteContainer])
    def test_website_container_driver(self, restful: WebsiteContainer):
        josh = restful.create('/users', json={'name': 'Josh Katz', 'testCasesWritten': 0})
        josh_resource = ('/users', josh['id'])

        # Josh exists after being created
        self.assertTrue(restful.exists(josh_resource))

        # Josh wrote 1 test case, lets update that
        updated = restful.update(josh_resource, json={'testCasesWritten': 1})

        # Make sure the update occured correctly
        self.assertEqual(updated['id'], josh['id'])
        self.assertEqual(updated['name'], josh['name'])
        self.assertEqual(updated['testCasesWritten'], 1)

        read = restful.get(josh_resource)

        # Update actually persisted the data to the database
        self.assertEqual(updated['id'], read['id'])
        self.assertEqual(updated['name'], read['name'])
        self.assertEqual(updated['testCasesWritten'], read['testCasesWritten'])

        restful.delete(josh_resource)

        # Delete persisted
        self.assertFalse(restful.exists(josh_resource))

        # Endpoints should return Not Found if the underling resource does not exist
        with self.assertRaises(ContainerRestNotFound):
            restful.get(josh_resource)

        with self.assertRaises(ContainerRestNotFound):
            restful.update(josh_resource, json={'testCasesWritten': 1})
```