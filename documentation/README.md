# Bunt User Manual

Bunt is a tool that:

* [Makes it simple to define the configuration of services](define-a-container.md),
* [Constructs a testing environment from those configurations](overview-of-bunt-internals.md), and
* [Define and executes a test suite within the test environment](workflow-of-bunt.md).

## Why Use Bunt?

Bunt allows you to do all of this on your local system. You don't need to send your
code to an eternal server to iteratively develop your project. You can share all of 
your integration tests with coworkers as, due to them being dockerized, they should
be fairly reproducible across systems. Bunt also comes with pre-made abstractions
for common operations so your tests can focus on the logic you are testing rather
than the way you kick off your logic.
