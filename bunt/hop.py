from bunt.containers import ContainerConfiguration
from os import getenv


class BuntContainer(ContainerConfiguration):
    image = getenv('BUNT_RUNNER_CONTAINER', 'bunt/bunt:latest')

    entry_point = ['bunt']
    command = []

    volume_mounts = {
        '/var/run/docker.sock': '/var/run/docker.sock'
    }
